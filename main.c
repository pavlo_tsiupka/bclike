//TODO: Make error messages text file
//TODO: Enable log with -v flag

#include <stdio.h>
#include <ctype.h>
#include <memory.h>


#include "utils.h"

#define MAX_NUMBER_LEN 19
#define MAX_TOKENS 255

char quit[] = "quit"; //TODO: Investigate, why made it const affect other const.

char *calc_tokens[MAX_TOKENS];

char buffer[MAX_TOKENS];


int main(void) {

    char supported_operands[SUPPORTED_OPERATOR_CNT + 1] = {0};
    supported_operands[SUPPORTED_OPERATOR_CNT] = 0;

    for (int operand = 0; operand < SUPPORTED_OPERATOR_CNT; operand++) {
        supported_operands[operand] = *operand_char_ptr[operand];
    }

    printf("This is a bc-like app. Try use it as a simple calculator. To exit - type quit. \n"
           "Supported data types is long only, so max value is %ld.\n"
           "Max number len is %ld digit.\n"
           "Supported operands are: %s \n",
           MAX_VALUE,
           MAX_NUMBER_LEN,
           supported_operands);

    while (1) {
        int error_found = 0;
        scanf("%s", buffer);

        if (*buffer == *quit) {
            return 0;
        }

        log_message("Processing %s ....\n", buffer);
        int tokens_count = find_tokens(buffer, calc_tokens);
        if (!tokens_count) continue;

        // Process operands one by one with priority
        for (int operand_id = 0; operand_id <= SUPPORTED_OPERATOR_CNT - 1; operand_id++) {

            if (!calc(operand_id, calc_tokens)) {
                error_found = 1;
                continue;
            }
            empty_arr(calc_tokens);

            for (int clc = 0; clc < MAX_TOKENS; clc++) {
                if (calc_tokens[clc] == NULL) {
                    break;
                }

                log_message("CC %s\n", calc_tokens[clc]);
            }

        }

        if (!error_found) printf("%s\n", calc_tokens[0]);
    }

}