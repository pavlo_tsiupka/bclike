#include "operators_func.h"

#define SUPPORTED_OPERATOR_CNT 4

#include <limits.h>

static const int MAX_TOKENS = 255;
static const long MAX_VALUE = LONG_MAX;

static const char csum = '+';
static const char cdiv = '/';
static const char csub = '-';
static const char cmul = '*';
static const char *operand_char_ptr[SUPPORTED_OPERATOR_CNT] = {&cmul, &cdiv, &csum, &csub};
static const char operations[SUPPORTED_OPERATOR_CNT + 1] = {csum, cdiv, csub, cmul, '\0'};
static const void *operand_func_ptr[SUPPORTED_OPERATOR_CNT] = {&fmul, &fdiv, &fsum, &fsub};


void empty_arr(char *arr[MAX_TOKENS]);

void log_message(char *message, ...);

int calc(int operand, char *token_collection[MAX_TOKENS]);

int find_tokens(char *buffer, char *token_collection[MAX_TOKENS]);



