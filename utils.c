#include <stdarg.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <ctype.h>

#include "utils.h"

#define LOGGING 0 //TODO: make log flag, or use build flag
#define MAX_TOKENS 255
#define MAX_NUMBER_LEN 19

void log_message(char *message, ...) {
    va_list argptr;
    va_start(argptr, message);
    if (LOGGING) {
        vfprintf(stdout, message, argptr);
    }
    va_end(argptr);

}

void empty_arr(char *arr[MAX_TOKENS]) {
    char *temp_arr[MAX_TOKENS] = {malloc(MAX_NUMBER_LEN * sizeof(char))};

    int copy_position = 0;

    for (int a = 0; a < MAX_TOKENS; a++) {
        if (arr[a] != NULL) {
            temp_arr[copy_position] = arr[a];
            copy_position++;
        }
    }

    memset(arr, 0, sizeof *arr);

    for (int ptr = 0; ptr <= MAX_TOKENS; ptr++) {
        arr[ptr] = temp_arr[ptr];
    }
}


int find_tokens(char *buffer, char *token_collection[MAX_TOKENS]) {

    int tokens_cnt = 0;
    int n_cnt = 0;
    char number[MAX_NUMBER_LEN] = {};

    log_message("Parsing tokens has started.");

    for (int chr = 0; buffer[chr] != 0; chr++) {

        if (isdigit(buffer[chr])) {
            number[n_cnt] = buffer[chr];
            if (!isdigit(buffer[chr + 1])) {

                if (strlen(number) > MAX_NUMBER_LEN) {
                    printf("Data error. Max number length is %d\n", MAX_NUMBER_LEN);
                    return 0;

                }

                token_collection[tokens_cnt] = malloc(MAX_NUMBER_LEN * sizeof(char));
                strcpy(token_collection[tokens_cnt], number);

                tokens_cnt++;
                memset(number, 0, sizeof number);
                n_cnt = 0;


            } else {
                n_cnt++;
            }
        } else if (strchr(operations, buffer[chr])) {

            token_collection[tokens_cnt] = malloc(1 * sizeof(char));
            strncpy(token_collection[tokens_cnt], &buffer[chr], 1);

            tokens_cnt++;
        } else {
            printf("Parse error. Char %c is not supported yet.\n", buffer[chr]);
            return 0;
        }
    }
    return tokens_cnt;
}


int calc(int operand, char *token_collection[MAX_TOKENS]) {
    long (*operand_func)(long, long);
    operand_func = operand_func_ptr[operand];

    log_message("--*-- Starting calculation with %i operation\n", operand);

    for (int clc = 0; token_collection[clc] != 0; clc++) {

        if (memcmp(token_collection[clc], operand_char_ptr[operand],
                   strlen(token_collection[clc]) * (int) sizeof(char)) == 0) {
            log_message("Token %s has been found.\n", operand_char_ptr[operand]);

            if ((!clc) || !token_collection[clc + 1]) {
                printf("Parse error. Syntax error.\n");
                return 0;
            }

            long m1, m2;

            sscanf(token_collection[clc - 1], "%ld", &m1);
            sscanf(token_collection[clc + 1], "%ld", &m2);

//            TODO: Is not working( Need investigate
//            memset(calc_tokens[clc - 1], 0, sizeof *calc_tokens[clc - 1]);
//            memset(calc_tokens[clc], 0, sizeof *calc_tokens[clc]);

            token_collection[clc - 1] = NULL;
            token_collection[clc] = NULL;

            long mr = operand_func(m1, m2); // magic here
            if (mr > MAX_VALUE) {
                printf("Data overflow. One of result more than %ld\n", LONG_MAX);
                return 0;
            }
            char res[MAX_NUMBER_LEN];
            snprintf(res, sizeof(res), "%ld", mr);
            strcpy(token_collection[clc + 1], res);


        } else {
            log_message("Token %s not found.\n", token_collection[clc]);
        }
    }
    return 1;
}
